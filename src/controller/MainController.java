package controller;

import model.TextModel;
import net.miginfocom.swing.MigLayout;
import org.omg.PortableServer.THREAD_POLICY_ID;
import view.ButtonView;
import view.GazeInputListener;
import view.MainView;
import view.TextView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Point2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Shrey on 4/4/2014.
 */
public class MainController implements PropertyChangeListener {

    MainView mainView;
    JTextArea textArea = new JTextArea();
    TextModel textModel;
    TextView textView;

    ButtonView buttonView;
    TextController textController;

    Point2D currentCursor = new Point2D.Float();
    Point2D currentGaze = new Point2D.Float();

    GazeInputListener gazeInputListener;


    PrintWriter writer;

    long timeToGaze;

    ArrayList<Long> times;
    ArrayList<String> id;
    ArrayList<String> modes;
    ArrayList<Integer> selectX;
    ArrayList<Integer> selectY;
    ArrayList<Float> GazeX;
    ArrayList<Float> GazeY;
    public String extension;


    public int mode = 1;

    boolean added = true;


    public MainController(MainView mainView) {
        this.mainView = mainView;
        init();
    }


    public void init() {

        buttonView = new ButtonView(this);

        this.times = new ArrayList<Long>();
        this.id = new ArrayList<String>();
        this.modes = new ArrayList<String>();
        this.selectX = new ArrayList<Integer>();
        this.selectY = new ArrayList<Integer>();
        this.GazeX = new ArrayList<Float>();
        this.GazeY = new ArrayList<Float>();
        mainView.setLayout(new MigLayout("fill", "[:250:250][ ][:250:250]", "[]"));
        textModel = new TextModel(false, null);
        textView = new TextView();
        textController = new TextController(textModel, textView);
        textController.setMainController(this);
        mainView.add(buttonView, "shrink");
        mainView.add(textView, "grow");
        textView.setVisible(true);
        mainView.pack();
        textModel.addPropertyChangeListener(this);


        gazeInputListener = new GazeInputListener();
        gazeInputListener.addPropertyChangeListener(this);

//            mouseMotionListener = new MouseMotionListener() {
//                @Override
//                public void mouseDragged(MouseEvent e) {
//
//                }
//
//                @Override
//                public void mouseMoved(MouseEvent e) {
////                    System.out.println(currentCursor.getX() + "   " + currentCursor.getY());
//
//                    currentCursor.setLocation(e.getX(), e.getY());
//                    if (textModel.isSelected() && textModel.getSelectedText().indexOf(" ") == -1) {
//                        if (currentCursor.getX() > mainView.getWidth() / 2 && currentCursor.getY() < mainView.getHeight() / 2) {
////                            System.out.println(currentCursor.getX() + "   " + currentCursor.getY());
//                            long duration = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - timeToGaze);
//                               if(added) {
//                                times.add(duration);
//                                id.add(textModel.getId());
//                                added =false;
//                            }
////                            System.out.println("Time " + duration);
//                            showMeaning();
//                        }
//                    } else {
//                        removeMeaning();
//                    }
//                }
//            };
//            mainView.addMouseMotionListener(this.mouseMotionListener);
        if (mode == 1)
            extension = "gaze";
        else if (mode == 2)
            extension = "mouseRightClick";


        Date date = new Date();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy_hh-mm-ss");
        String formattedDate= df.format(date);
        System.out.printf(formattedDate);
        File file = new File("data/output/");
        if (!file.isDirectory()) {
            file.mkdirs();
            file = new File("data/output/");
        }
        String fileName;
        if (file.isDirectory()) {
            File[] names = file.listFiles();

            fileName = "data/output/" + "test" + formattedDate + ".csv";
        } else fileName = "data/output/" + "test" +formattedDate + ".csv";
        try {
            writer = new PrintWriter(fileName);
            writer.println("id" + ";" + "Millis" + ";" + "Modes" +";" + "distance");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }


    public void showMeaning() {
        mainView.remove(textArea);
        textArea.setVisible(true);
        textArea.setLineWrap(true);
        textArea.setEditable(false);
        mainView.add(textArea, "grow");
        textArea.setText(textModel.getSelectedText()+": 42");
        this.mainView.invalidate();
        this.mainView.repaint();
    }

    public void removeMeaning() {
        this.added = true;
        mainView.remove(textArea);
        this.mainView.invalidate();
        this.mainView.repaint();

    }


    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName().equals("gaze_changed")) {

            Point2D object = (Point2D) evt.getNewValue();
            this.currentGaze.setLocation(object);
//            System.out.println(object.getX()+"  "+object.getY());

            if (this.textModel.isSelected() && textModel.getSelectedText().indexOf(" ") == -1 ) {
                if(mode == 1) {
                    if (this.currentGaze.getX() > 0.5 && this.currentGaze.getY() < 0.5) {
//                     System.out.println(this.currentCursor.getX() + "   " + this.currentCursor.getY());
                        long duration = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - timeToGaze);

                        if (this.added) {
                            showMeaning();

                            Point point = SwingUtilities.convertPoint(this.textController.getSelectedArea()
                                    , (int) textController.getPoint2D().getX()
                                    , (int) textController.getPoint2D().getY()
                                    , this.mainView);
                            this.selectX.add((int)point.getX());
                            this.selectY.add((int)point.getY());

                            this.GazeX.add((float)this.currentGaze.getX());
                            this.GazeY.add((float)this.currentGaze.getY());
                            times.add(duration);
                            modes.add(extension);
                            id.add(textModel.getId());
                            this.added = false;

                        }
//                        System.out.println("Time gaze " + duration);

                    }

                }
                if(mode == 2) {
                    if (this.currentGaze.getX() > 0.5 && this.currentGaze.getY() < 0.5 && textController.isRightClicked() && textModel.getSelectedText().indexOf(" ") == -1  ) {
//                     System.out.println(this.currentCursor.getX() + "   " + this.currentCursor.getY());
                        long duration = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - timeToGaze);
                        if (this.added) {

                            Point point = SwingUtilities.convertPoint(this.textController.getSelectedArea()
                                    , (int) textController.getPoint2D().getX()
                                    , (int) textController.getPoint2D().getY()
                                    , this.mainView);
                            this.selectX.add((int)point.getX());
                            this.selectY.add((int)point.getY());

                            this.GazeX.add((float) this.currentGaze.getX());
                            this.GazeY.add((float)this.currentGaze.getY());
                            times.add(duration);
                            id.add(textModel.getId());
                            modes.add(extension);
                            this.added = false;
                        }
//                        System.out.println("Time " + duration);
                    }
                }

            }
        }

        if (evt.getPropertyName().equals("Selection")) {
            boolean value = (Boolean) evt.getNewValue();
            if (value == false) {
                removeMeaning();
            }
        }

        if (evt.getPropertyName().equals("NanoTime")) {
            this.timeToGaze = (Long) evt.getNewValue();
        }
    }

    public void closeFile() {

        for (int i = 0; i < this.times.size(); i++) {
            String s = this.id.get(i);
            long l = this.times.get(i);
            String mode = this.modes.get(i);
            Point gazePosNormal = new Point((int)(this.currentGaze.getX() * java.awt.Toolkit.getDefaultToolkit().getScreenSize().getWidth()), (int) (this.currentGaze.getY() * java.awt.Toolkit.getDefaultToolkit().getScreenSize().getHeight()));
//            System.out.println("distnace  " + gazePosNormal.getX() + " ; " + gazePosNormal.getY());
//            double yDiff = this.GazeY.get(i)-this.selectY.get(i);
//            double xDiff = (this.GazeX.get(i)-this.selectX.get(i));
//            double distance = Math.sqrt( yDiff * yDiff + xDiff*xDiff );
            double distance = gazePosNormal.distance(new Point(this.selectX.get(i), this.selectY.get(i)));

            writer.print(s + ";");
            writer.print(l + ";");
            writer.print(mode + ";");
//            writer.print(this.selectX.get(i) + ";");
//            writer.print(this.selectY.get(i) + ";");
//            writer.print(this.GazeX.get(i) + ";");
//            writer.print(this.GazeY.get(i) + ";" );
            writer.println(distance);



        }
        writer.close();
    }

}
