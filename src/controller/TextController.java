package controller;

import model.TextModel;
import view.Gaze;
import view.TextView;

import javax.swing.*;
import java.awt.geom.Point2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

/**
 * Created by Shrey on 4/4/2014.
 */
public class TextController {

    MainController mainController;
    TextModel textModel;
    TextView textView;


    public TextController(TextModel textModel, TextView textView) {
        this.textModel = textModel;
        this.textView = textView;
        this.textView.setTextController(this);

    }

    public void setSelectedText(String s) {
        this.textModel.setSelectedText(s);
    }


    public void setSelected(boolean value) {

        this.textModel.setSelected(value);

    }


    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }


    public ArrayList<String> getQuestionList() {
        return this.textModel.getQuestionList();
    }

    public ArrayList<String> getIndexList() {
        return this.textModel.getIndexList();
    }

    public String getId() {
        return this.textModel.getId();
    }

    public void setId(String id) {
        this.textModel.setId(id);
    }

    public boolean isRightClicked() {
        return this.textModel.isRightClicked();
    }

    public void setRightClicked(boolean rightClicked) {
        this.textModel.setRightClicked(rightClicked);
        if (rightClicked == true && mainController.mode == 2 &&  textModel.getSelectedText().indexOf(" ") == -1) {
            mainController.showMeaning();
        }
    }

    public Point2D getPoint2D() {
        return this.textModel.getPoint2D();
    }

    public void setPoint2D(Point2D point2D) {
        this.textModel.setPoint2D(point2D);
    }

    public JTextArea getSelectedArea() {
        return this.textModel.getSelectedArea();
    }

    public void setSelectedArea(JTextArea selectedArea) {
        this.textModel.setSelectedArea(selectedArea);
    }
}
