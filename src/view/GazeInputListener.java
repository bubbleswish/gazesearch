package view;

import java.awt.geom.Point2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * Created by Shrey on 4/7/2014.
 */
public class GazeInputListener {


    Point2D.Float coordinates =  new Point2D.Float(0.0f,0.0f) ;

    boolean gazePresent = false;

    private PropertyChangeSupport propertyChangeSupport =
            new PropertyChangeSupport(this);

    public GazeInputListener() {

        Gaze gaze = new Gaze();
        gaze.setGazeInputListener(this);
        gaze.startGaze();

        if (gaze.getTrackerThread() != null) {
            gazePresent = true;
        }


    }

    /*
     * @param Point2D the updated Gaze point
     */
    public void update(Point2D points) {
        Point2D.Float old = new Point2D.Float((float)this.coordinates.getX(),(float) this.coordinates.getY());
        this.coordinates.setLocation(points.getX(), points.getY());
        propertyChangeSupport.firePropertyChange("gaze_changed", old, this.coordinates);


    }

    public void
    addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void
    removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }


    public boolean isGazePresent() {
        return gazePresent;
    }
}