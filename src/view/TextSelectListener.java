package view;

import controller.TextController;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by Shrey on 4/12/2014.
 */
public class TextSelectListener implements MouseListener{
    JTextArea jTextArea;
    String id;
    TextController textController;


    public TextSelectListener(JTextArea jTextArea , TextController textController, String id) {
        this.jTextArea = jTextArea;
        this.textController = textController;
        this.id = id;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
//        System.out.println("mouse  "+e.getX()+"   "+e.getY());

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

        if(e.getButton() == MouseEvent.BUTTON1) {
            if (this.jTextArea.getSelectedText() != null) {
                textController.setSelected(true);
                textController.setSelectedText(this.jTextArea.getSelectedText());
                textController.setId(this.id);
                try {

                    Rectangle rectangle1 = this.jTextArea.modelToView(this.jTextArea.getSelectionStart());
                    Rectangle rectangle2 = this.jTextArea.modelToView(this.jTextArea.getSelectionEnd());

                    textController.setPoint2D(new Point(
                            (int)(rectangle1.getX() + (rectangle2.getMaxX() - rectangle1.getX() )/2)
                    ,(int)(rectangle1.getY() + (rectangle2.getMaxY() - rectangle1.getY() )/2) ));

                    textController.setSelectedArea(this.jTextArea);

                } catch (BadLocationException e1) {
                    e1.printStackTrace();
                }

            } else {
                textController.setSelected(false);
//                        System.out.println("nothing");
            }
        }

        else if( e.getButton() == MouseEvent.BUTTON3)
        {
            if (this.jTextArea.getSelectedText() != null ) {
                textController.setRightClicked(true);


            }
            else
            {
                textController.setRightClicked(false);
            }
        }
        else {
            textController.setSelected(false);
            textController.setRightClicked(false);
        }
    }
    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
