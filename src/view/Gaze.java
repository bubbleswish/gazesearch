package view;


import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.awt.geom.Point2D;
import java.io.*;
import java.net.*;
import java.util.Queue;

/**
 * Created with IntelliJ IDEA.
 * User: Shrey
 * Date: 10/20/13
 * Time: 4:01 AM
 * To change this template use File | Settings | File Templates.
 * Thanks to Michael McGuffin (http://profs.etsmtl.ca/mmcguffin/)
 * for the framework of animation code.
 *
 */

public class Gaze implements Runnable {


    // variables required if using moving averages
    private Queue<Point2D> gazePosNormalAveraging;
    private Point2D gazePosNormalYAverage = new Point2D.Float();
    private Point2D gazePosNormalYAveringTemp = new Point2D.Float();
    private Point2D gazePosInvertY = new Point2D.Float();


    private Point2D gazePosNormalY = new Point2D.Float();

    GazeInputListener gazeInputListener;

    Thread trackerThread = null;
    boolean isTrackerThreadSuspended = true;

    public static final int NUM_FRAMES_PER_SECOND = 30;


    public String address = "127.0.0.1";
    public int port = 4242;


    InputStream ios ;
    DataInputStream reader;

    OutputStream os;
    DataOutputStream writer;

    boolean gazePosValid;


    private Socket socket;

    private String connectionParams =

            "<SET ID=\"ENABLE_SEND_DATA\" STATE=\"1\"/>\r\n" +
            "<SET ID=\"ENABLE_SEND_POG_BEST\" STATE=\"1\"/>\r\n";

    boolean isTrackerActive = false;

    public Gaze() {

    }

    public void startBackgroundWork() {
        if ( trackerThread == null ) {
            trackerThread = new Thread( this );
            isTrackerThreadSuspended = false;
            trackerThread.start();
        }
        else {
            // The the thread already exists; we simply need to "wake it up"
            if (isTrackerThreadSuspended) {
                isTrackerThreadSuspended = false;
                synchronized( this ) {
                    notify();
                }
            }
        }
    }
    public void stopBackgroundWork() {
        isTrackerThreadSuspended = true;
    }

    public void run() {
        try {
            while (true) {
                synchronized( this ) {

                    if (isTrackerActive) {


                            if(socket != null)
                            {
                                int size = 0;
                                try {

                                    size = socket.getReceiveBufferSize();
                                    byte[] received = new byte[size];
                                    reader.read(received, 0, size);
                                    String xmlString = new String(received, "US-ASCII");

                                    xmlString = xmlString.substring(0, xmlString.lastIndexOf('>') + 1);
                                    xmlString = "<?xml version=\"1.0\"?><doc>" + xmlString;
                                    xmlString = xmlString + "</doc>";

                                    DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
                                    DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
                                    Document doc = docBuilder.parse(new InputSource(new StringReader(xmlString)));

                                    doc.getDocumentElement().normalize();
                                    NodeList rec = doc.getElementsByTagName("REC");

                                    if(rec != null )
                                    {
                                      String pogX = rec.item(rec.getLength()-1).getAttributes().getNamedItem("BPOGX").getNodeValue();
                                      String pogY = rec.item(rec.getLength()-1).getAttributes().getNamedItem("BPOGY").getNodeValue();
                                      String pogV = rec.item(rec.getLength()-1).getAttributes().getNamedItem("BPOGV").getNodeValue();


                                        gazePosValid = pogV.equals("1")?true:false;

                                        if (gazePosValid) {
                                            gazePosNormalY = new Point2D.Float(Float.parseFloat(pogX) , (Float.parseFloat(pogY) ));
                                            gazeInputListener.update(gazePosNormalY);
                                             //uncomment if using moving averages
//                                            gazePosNormalY = new Point2D.Float((float) (Float.parseFloat(pogX) * java.awt.Toolkit.getDefaultToolkit().getScreenSize().getWidth()), (float) ((Float.parseFloat(pogY) * java.awt.Toolkit.getDefaultToolkit().getScreenSize().getHeight()) ));
//                                            gazePosInvertY = new Point2D.Float((float) (Float.parseFloat(pogX) * java.awt.Toolkit.getDefaultToolkit().getScreenSize().getWidth()), (float) ((java.awt.Toolkit.getDefaultToolkit().getScreenSize().getHeight() - Float.parseFloat(pogY)*java.awt.Toolkit.getDefaultToolkit().getScreenSize().getHeight())));
//                                            gazePosNormalAveraging.add(gazePosNormalY);
//                                            gazePosNormalYAveringTemp = new Point2D.Float((float)(gazePosNormalYAveringTemp.getX() +
//                                                    gazePosNormalY.getX()), (float)(gazePosNormalYAveringTemp.getY() + gazePosNormalY.getY()));
//                                            System.out.println(gazePosNormalY.getX()+"   "+gazePosNormalY.getY()+"   "+pogV);

//                                            if(gazePosNormalAveraging.size() > 10) {
//                                                Point2D old = gazePosNormalAveraging.remove();
//                                                gazePosNormalYAveringTemp = new Point2D.Float((float)(gazePosNormalYAveringTemp.getX() -
//                                                        gazePosNormalY.getX()), (float)(gazePosNormalYAveringTemp.getY() - gazePosNormalY.getY()));
//                                                        gazePosNormalYAverage =
//                                                        new Point2D.Float((float)gazePosNormalYAveringTemp.getX()/gazePosNormalAveraging.size(),
//                                                                (float)gazePosNormalYAveringTemp.getY()/gazePosNormalAveraging.size());
//                                                System.out.println(gazePosNormalYAverage.getX()+"    "+gazePosNormalYAverage.getY());
//
//                                            }

                                        }
                                    }



                                } catch (Exception e1) {
                                    e1.printStackTrace();
                                }

                        }


                        }
                }

                // Now the thread checks to see if it should suspend itself
                if (isTrackerThreadSuspended) {
                    synchronized( this ) {
                        while (isTrackerThreadSuspended) {
                            wait();
                        }
                    }
                }

                trackerThread.sleep((long)
                        1000.0f / NUM_FRAMES_PER_SECOND   // sleepIntervalInMilliseconds
                );

            }
        }
        catch (InterruptedException e) { }
    }


    /**
     * runs the first time to initialze
     * gaze tracker, opens up the socket
     * opens up the input and ouptut stream
     */
    public void startGaze() {
        isTrackerActive = true;

        try {

                InetAddress ip = InetAddress.getByName(address);
                socket = new Socket(ip, port);
                byte[] myBytes = connectionParams.getBytes("US-ASCII");
                os = socket.getOutputStream();
                writer = new DataOutputStream(os);
                ios = socket.getInputStream();
                reader = new DataInputStream(ios);
                writer.write(myBytes, 0, myBytes.length);

            gazePosNormalY = new Point2D.Float();

            // required in moving averages
//            gazePosInvertY = new Point2D.Float();
//            gazePosNormalYAveringTemp= new Point2D.Float();
//            gazePosNormalAveraging = new LinkedList<Point2D>();
            startBackgroundWork();


        }
        catch(Exception e )
        {
            System.out.println("Socket not open");
            e.printStackTrace();
        }

    }

    public Thread getTrackerThread() {
        return trackerThread;
    }

    public void setGazeInputListener(GazeInputListener gazeInputListener) {
        this.gazeInputListener = gazeInputListener;
    }
}


