package view;

import controller.MainController;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Shrey on 4/12/2014.
 */
public class ButtonView extends JPanel {

    JButton jButton1;

    JButton jButton2;

    MainController mainController;

    public ButtonView(final MainController mainController) {
        this.mainController = mainController;
        this.setLayout(new MigLayout());
        jButton1 = new JButton();
        jButton1.setText("Gaze");
        jButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                    mainController.mode = 1;
                    mainController.extension = "gaze";
                mainController.removeMeaning();

                System.out.println("1");


            }
        });
        jButton1.setVisible(true);
        jButton2 = new JButton();
        jButton2.setText("mouseRightClick");
        jButton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                mainController.mode = 2;
                mainController.extension = "mouseRightClick";
                mainController.removeMeaning();
                System.out.println("2");


            }
        });
        jButton2.setVisible(true);

        this.add(jButton1,"wrap");
        this.add(jButton2);
        this.setVisible(true);

    }
}
