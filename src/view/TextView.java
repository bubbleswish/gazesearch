package view;

import controller.TextController;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.util.ArrayList;

/**
 * Created by Shrey on 4/4/2014.
 */
public class TextView extends JPanel {

    ArrayList<JTextArea> textArea;
    TextController textController;

    public TextView() {
        this.setVisible(true);
        this.setLayout(new MigLayout("fill"));
        this.textArea = new ArrayList<JTextArea>();
    }


    public void setTextController(TextController textController) {
        this.textController = textController;
        this.textArea = new ArrayList<JTextArea>();

        for (int i = 0; i < this.textController.getQuestionList().size(); i++) {
            JTextArea jtextArea = new JTextArea();
            jtextArea.setText(this.textController.getIndexList().get(i)
                    +" "
                    +this.textController.getQuestionList().get(i));
            this.add(jtextArea,"grow , wrap");
            jtextArea.setLineWrap(true);
            jtextArea.setEditable(false);
            jtextArea.setVisible(true);
            jtextArea.addMouseListener(new TextSelectListener(jtextArea, textController, this.textController.getIndexList().get(i)));
        }

    }


}
