package view;

import controller.MainController;
import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by Shrey on 4/4/2014.
 */
public class MainView extends JFrame {


    MainController mainController;

    public MainView() {
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        this.setSize(new Dimension(400,400));
    }

    public MainController getMainController() {
        return mainController;

    }

    public void setMainController(final MainController mainController) {
        this.mainController = mainController;
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                mainController.closeFile();

            }
        });
    }


}
