package model;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.awt.geom.Point2D;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.util.ArrayList;

/**
 * Created by Shrey on 4/4/2014.
 */
public class TextModel  {

    boolean isSelected;
    String selectedText;

    ArrayList<String> questionList;
    ArrayList<String> indexList;

    String id;

    NodeList index;
    NodeList question;

    File file;

    boolean rightClicked = false;

    Point2D point2D;

    JTextArea selectedArea;

    private PropertyChangeSupport propertyChangeSupport =
            new PropertyChangeSupport(this);

    long selectionTimer;

    public TextModel(boolean isSelected, String selectedText) {
        this.isSelected = isSelected;
        this.selectedText = selectedText;
        this.selectionTimer = System.nanoTime();

        try {
            file = new File("data/input/questions.xml");
        } catch (Exception e) {
            System.out.println("Input File Exception");
        }
        readQuestions();
    }

    void readQuestions() {
        this.questionList = new ArrayList<String>();
        this.indexList = new ArrayList<String>();

        try {

            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(file);
            // normalize text representation
            doc.getDocumentElement().normalize();
            index = doc.getElementsByTagName("id");
            question = doc.getElementsByTagName("word");

            for (int i = 0; i < index.getLength(); i++) {
                String id  = index.item(i).getTextContent();
                String word  = question.item(i).getTextContent();

                this.indexList.add(id);
                this.questionList.add(word);
            }

        } catch (SAXParseException err) {
            System.out.println("** Parsing error" + ", line "
                    + err.getLineNumber() + ", uri " + err.getSystemId());
            System.out.println(" " + err.getMessage());

        } catch (SAXException e) {
            Exception x = e.getException();
            ((x == null) ? e : x).printStackTrace();

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }


    public String getSelectedText() {
        return selectedText;
    }

    public void setSelectedText(String selectedText) {
        this.selectedText = selectedText;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public boolean isOneWordSelected() {
        return isSelected;
    }

    public void setSelected (boolean value){
        this.isSelected = value;
        this.selectionTimer = System.nanoTime();
        if(value == false) this.propertyChangeSupport.firePropertyChange("Selection",!value,value);
        if(value == false) this.propertyChangeSupport.firePropertyChange("NanoTime",0,selectionTimer);

    }

    public void
    addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void
    removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }
    public ArrayList<String> getQuestionList() {
        return questionList;
    }

    public void setQuestionList(ArrayList<String> questionList) {
        this.questionList = questionList;
    }

    public ArrayList<String> getIndexList() {
        return indexList;
    }

    public void setIndexList(ArrayList<String> indexList) {
        this.indexList = indexList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isRightClicked() {
        return rightClicked;
    }

    public void setRightClicked(boolean rightClicked) {
        this.rightClicked = rightClicked;
    }

    public Point2D getPoint2D() {
        return point2D;
    }

    public void setPoint2D(Point2D point2D) {
        this.point2D = point2D;
    }

    public JTextArea getSelectedArea() {
        return selectedArea;
    }

    public void setSelectedArea(JTextArea selectedArea) {
        this.selectedArea = selectedArea;
    }
}


