import controller.MainController;
import view.MainView;

import javax.swing.*;

/**
 * Created by Shrey on 4/4/2014.
 */
public class RunApplication {

    public static void main(String[] args) {


        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {

                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

                } catch (Exception e) {
                }

                MainView mainView = new MainView();
                MainController mainController = new MainController(mainView);
                mainView.setMainController(mainController);

            }
        });
    }
}
